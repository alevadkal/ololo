#include <iostream>
#include <memory>

class A
{
public:
    A(){
        std::cout <<"constructor A:"<< (unsigned long)this <<std::endl;
    }
    ~A(){
        std::cout <<"destructor A:"<< (unsigned long)this <<std::endl;
    }

    int a;

};

class B
{
    std::shared_ptr<A> a;
public:
    B(int b=0):
        a(new A())
    {
        std::cout <<"constructor B:"<< (unsigned long)this <<std::endl;
        a->a=b;

    }

    B(const B& b){
        std::cout <<"copy constructor B:"<< (unsigned long)this <<std::endl;
        this->a=b.a;
    }
    
    B& operator =(const B& b){
        std::cout <<"opertator = B:"<< (unsigned long)this <<std::endl;
        this->a=b.a;
    }

    int get()
    {
        return a->a;
    }

    ~B(){
        std::cout <<"destructor B:"<< (unsigned long)this <<std::endl;
    }

};

int getData()
{
    std::cout <<"getData-1"<<std::endl;
    B b(1);
    std::cout <<"getData0"<<std::endl;
    B b1(b);
    std::cout <<"getData1"<<std::endl;
    int result = b.get();
    std::cout <<"getData2"<<std::endl;
    b = B(5);
    std::cout <<"getData3"<<std::endl;
    return result + b.get()+b1.get();
}


int main()
{
    std::cout <<"main0"<<std::endl;
    std::cout << getData() <<std::endl;
    std::cout <<"main1"<<std::endl;
    return 0;
}